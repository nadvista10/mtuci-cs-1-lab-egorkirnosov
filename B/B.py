

portion_down_payment = 0.25
current_saving = 0
current_month = 0
r = 0.04

annual_salary = int(input('Enter your starting annual salary '))
portion_saved = float(input('Enter the percent of your salary to sace, as decimal '))
total_cost = int(input('Enter the cost of your dream house '))
semi_annual_raise = float(input('Enter the semi-annual raise, as decimal '))

down_payment_cost = total_cost * portion_down_payment

def UpdateSalary():
    global monthly_saved
    global monthly_salary
    global annual_salary

    monthly_salary = annual_salary / 12
    monthly_saved = monthly_salary * portion_saved

def NextMonth():
    global current_month
    global current_saving
    global semi_annual_raise
    global annual_salary

    current_saving += current_saving * r / 12
    current_saving += monthly_saved
    current_month += 1
    if (current_month % 6 == 0):
        annual_salary += annual_salary*semi_annual_raise
        UpdateSalary()
    

UpdateSalary()

while(current_saving < down_payment_cost): 
    NextMonth()


print(f'Number of months: {current_month}')
